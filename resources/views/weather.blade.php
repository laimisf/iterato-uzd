<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Weather App</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// --><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>

<div class="container">
    <h2>Weather app</h2>

    <div class="jumbotron">
        <form id="add_city_form">
            <div class="form-group">
                <label for="api_key">API Key</label>
                <input type="text" class="form-control" id="api_key" placeholder="API Key" value="5ef5d91dac597ca7e7de90b556cbe1e1" required>
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" id="city" placeholder="City" required>
            </div>
            <button type="submit" class="btn btn-default">Add City</button>
        </form>
    </div>

    <div id="alerts"></div>

    <ul class="nav nav-tabs" id="city_tabs">
        <li class="active"><a data-toggle="tab" href="#home"><strong>PAAIŠKINIMAI</strong></a></li>
    </ul>

    <div class="tab-content" id="city_tabs_content">
        <div id="home" class="tab-pane fade in active">
            <h3>Kaip naudotis?</h3>
            <p>Norėdami pridėti miestą, įrašykite miesto pavadinimą ir spauskite "Add City".</p>
            <h3>Papildoma</h3>
            <ul>
                <li>Jeigu miestas kartojasi, tai pašalinamas prieš tai įkeltas tab'as. Taip išvengiam pasikartojančių tab'ų.</li>
                <li>Rodomos tiek pavykusios operacijos, tiek klaidos, tokios kaip neteisingas API raktas, ar pvz nerandamas įvestas miestas (galit pabandyti pakeisti API raktą arba įvesti bet kokį neegzistuojantį miestą)</li>
            </ul>
        </div>
    </div>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script>
    $(document).ready(function () {

        //removes a tab
        function removeCity(cityname) {
            $("[data-remove-city='" + cityname + "']").click(function () {
                $("[data-city-name='" + cityname + "']").remove();
                $("#city_tabs li a").last().tab('show');
            });
        }

        $("#add_city_form").submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: "{{ route('api_call') }}",
                data: {
                    city: $("#city").val(),
                    apiKey: $("#api_key").val()
                },
                type: "GET",
                dataType: "html",

                beforeSend: function () {
                },

                success: function (data) {

                    if( $(data).find("#alert-success").length ) {
                        var cityname = $(data).find("#city_name").text();
                        //checks for duplicate cities and removes them
                        $("[data-city-name='" + cityname + "']").remove();

                        //adds new city
                        $("#city_tabs").append($(data).find("[data-li-city]"));
                        $("#city_tabs_content").append($(data).find("[data-tab-pane]"));
                        $("#city_tabs li a").last().tab('show');

                        //adds remove city event listener to newly added tab
                        removeCity(cityname);

                        $("#alerts").html("").append($(data).find("#alert-success"));
                        $("#city").val("");
                    } else {
                        $("#alerts").html("").append($(data).find("#alert-failure"));
                    }

                }
            });
        });

    });
</script>

</body>
</html>