<div>

    @if ($weatherData['cod'] == 200)

        <li data-li-city data-city-name="{{ $weatherData['name'] }}">
            <a data-toggle="tab" href="#{{ $weatherData['name'] }}">{{ $weatherData['name'] }}
                <button class="close" type="button" title="Remove {{ $weatherData['name'] }}" style="margin-left: 10px;" data-remove-city="{{ $weatherData['name'] }}">×</button>
            </a></li>


        <div data-tab-pane data-city-name="{{ $weatherData['name'] }}" class="tab-pane fade" id="{{ $weatherData['name'] }}">
            <h3>{{ $weatherData['name'] }}</h3>
            <p>Weather: {{ $weatherData['weather'][0]['main'] }} ({{ $weatherData['weather'][0]['description'] }})<br>
            Temperature: {{ $weatherData['main']['temp'] }} &#8451;<br>
            Pressure: {{ $weatherData['main']['pressure'] }} kPa
        </div>


        <span id="city_name">{{ $weatherData['name'] }}</span>


        <div class="alert alert-success alert-dismissable" id="alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><span data-span-city>{{ $weatherData['name'] }}, {{ $weatherData['sys']['country'] }}</span></strong> was successfully found and added!
        </div>

    @else

        <div class="alert alert-danger alert-dismissable" id="alert-failure">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><span data-span-city>Error!</span></strong> Details: {{ $weatherData['message'] }}
        </div>

    @endif

</div>

