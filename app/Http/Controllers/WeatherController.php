<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeatherController extends Controller
{

    public function indexPage()
    {
        return view('weather');
    }


    public function apiCall(Request $request)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://api.openweathermap.org/data/2.5/weather?units=metric&q=". $request->city . "&appid=" . $request->apiKey
        ));
        $weatherData = json_decode(curl_exec($curl), true);
        
        return view("api_result", ['weatherData' => $weatherData]);
    }
}
